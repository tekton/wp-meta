Tekton WP-Meta
==================

A simple component that adds a Meta Tags metabox to Wordpress posts that lets you manually define meta keywords and description.
