<?php namespace Tekton\Wordpress\Meta;

use Tekton\Support\Contracts\ObjectCaching;

class MetaManager implements ObjectCaching {

    use \Tekton\Support\Traits\ObjectPropertyCache;

    protected $postTypes = ['page', 'post'];

    function defaultPostTypes() {
        return $this->postTypes;
    }

    function has($key, $id = null) {
        return ($this->get($key, null, $id)) ? true : false;
    }

    function get($key, $default = null, $id = null) {
        if ($this->cache_exists($key)) {
            $meta = $this->cache_get($key);
        }
        else {
            $meta = $this->cache_set(post_meta('meta', $key, $id));
        }

        return ($meta) ? $meta : $default;
    }
}
