<?php namespace Tekton\Wordpress\Meta\Facades;

class Meta extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'wp.meta'; }
}
