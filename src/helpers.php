<?php

if (! function_exists('meta')) {
    /**
     * Get / set the specified meta value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param  array|string  $key
     * @param  mixed  $default
     * @return mixed
     */
    function meta($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('wp.meta');
        }

        return app('wp.meta')->get($key, $default);
    }
}

function post_meta($group, $key, $id = null) {
    if (is_null($id)) {
        $id = get_the_ID();
    }

    return get_post_meta($id, meta_key($group, $key), true);
}

function term_meta($group, $key, $id) {
    return get_term_meta($id, meta_key($group, $key), true);
}
