<?php namespace Tekton\Wordpress\Meta\Providers;

use Tekton\Support\ServiceProvider;
use Tekton\Wordpress\Meta\MetaManager;

class MetaProvider extends ServiceProvider {

    function register() {
        $this->app->singleton('wp.meta', function() {
            return new MetaManager();
        });
    }

    function boot() {
        add_action('metabox_init', function () {
            $meta = app('wp.meta');

            $meta_tags = create_metabox( array(
                'id'            => 'meta_tags',
                'title'         => __( 'Meta Tags', 'tekton-wp-meta' ),
                'object_types'  => apply_filters('meta_tags_post_types', $meta->defaultPostTypes()), // Post type
                'context'       => 'normal',
                'priority'      => 'default',
                'show_names'    => true, // Show field names on the left
                'closed'        => true, // Keep the metabox closed by default
            ));

            $description = $meta_tags->add_field( array(
                'name' => __( 'Description', 'tekton-wp-meta' ),
                'id'   => meta_key('meta', 'description'),
                'type' => 'textarea_small',
                'desc' => 'A flowing text that describes/summarizes this page.',
                'sanitization_cb' => 'sanitize_text_field',
            ));

            $keywords = $meta_tags->add_field( array(
                'name' => __( 'Keywords', 'tekton-wp-meta' ),
                'id'   => meta_key('meta', 'keywords'),
                'type' => 'text',
                'desc' => 'A comma separated list of keywords that describes this page.',
                'sanitization_cb' => 'sanitize_text_field',
            ));

            if (is_admin_edit()) {
                $grid = create_grid($meta_tags);
                $row = $grid->addRow();
                $row->addColumns(array($description));

                $row = $grid->addRow();
                $row->addColumns(array($keywords));
            }
        });
    }
}
